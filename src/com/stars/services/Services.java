package com.stars.services;

public class Services {

    //1
    public void outSevenBySeven() {
        for(int i=0; i<7; i++){
            for(int j=0; j<7; j++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    //2
    public void outEmptySquare() {
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 0 || i == 6 || j == 0 || j == 6) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    //3
    public void outTriangleLeftUp() {
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 0 || j == 6 - i || j == 0) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    //4
    public void outTriangleLeftDown() {
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 6 || j == 0 || j == i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    //5
    public void outTriangleRightDown() {
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 6 || j == 6 || j == 6 - i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    //6
    public void outTriangleRightUp() {
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 0 || i == j || j == 6) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    //7
    public void outCross() {
            for(int i = 0; i < 7; i++){
                for(int j = 0; j < 7; j++){
                    if (i == j || j == 6 - i) {
                        System.out.print("* ");
                    } else {
                        System.out.print("  ");
                    }
                }
                System.out.println();
            }
    }
    //8
    public void outTriangleUp() {
            for(int i = 0; i < 4; i++){
                for(int j = 0; j < 7; j++){
                    if (i == 0 || j == 6 - i || j == i) {
                        System.out.print("* ");
                    } else {
                        System.out.print("  ");
                    }
                }
                System.out.println();
            }
    }
    //9
    public void outTriangleDown() {
        for(int i = 3; i < 7; i++){
            for(int j = 0; j < 7; j++){
                if (i == 6 || j == 6 - i || j == i) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

}




